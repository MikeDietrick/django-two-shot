from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt

# Register your models here.


@admin.register(ExpenseCategory)
class ExpenseCategory(admin.ModelAdmin):
    display = (
        "name",
        "owner",
    )


@admin.register(Account)
class Account(admin.ModelAdmin):
    display = (
        "name",
        "number",
    )


@admin.register(Receipt)
class Receipt(admin.ModelAdmin):
    display = (
        "vendor",
        "total",
        "tax",
        "date",
    )
